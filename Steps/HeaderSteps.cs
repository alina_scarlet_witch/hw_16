﻿using hw_16.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using OpenQA.Selenium.Interactions;
using TechTalk.SpecFlow;

namespace hw_16.Steps
{
    [Binding]
    public class HeaderSteps
    {
        public IWebDriver driver;
        public MainPage mainPage;

        [BeforeScenario]
        public void CreateDriver()
        {
            driver = new ChromeDriver(@"C:\");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMinutes(5);
        }

        [Given(@"https://allo\.ua website is open")]
        public void GivenHttpsAllo_UaWebsiteIsOpen()
        {
            driver.Navigate().GoToUrl("https://allo.ua");
            driver.Manage().Window.Maximize();
            mainPage = new MainPage(driver);
            //authorizationForm = new AuthorizationForm(driver);
           // ScenarioContext.Current.Pending();
        }

        [Given(@"https://allo\.ua website is open on location of the city Kyiv")]
        public void GivenHttpsAllo_UaWebsiteIsOpenOnLocationOfTheCityKyiv()
        {
            mainPage.ClickLocationButtonOnMainPage().
                     ClickLocationKievButtonOnMainPage();

           // ScenarioContext.Current.Pending();
        }

        [Then(@"The location of the city has Kyiv")]
        public void ThenTheLocationOfTheCityHasKyiv()
        {
            string mainLocationLabel = mainPage.GetTextMainLocationLabel();
            Assert.AreEqual("Київ", mainLocationLabel);
            //ScenarioContext.Current.Pending();
        }

        [When(@"User clicks on location of the city")]
        public void WhenUserClicksOnLocationOfTheCity()
        {
            mainPage.ClickLocationButtonOnMainPage();
            //ScenarioContext.Current.Pending();
        }

        [When(@"User clicks on the city Dnepr")]
        public void WhenUserClicksOnTheCityDnepr()
        {
            mainPage.ClickLocationDneprButtonOnMainPage();
            //ScenarioContext.Current.Pending();
        }

        [Then(@"The location of the city has changed to the Dnepr")]
        public void ThenTheLocationOfTheCityHasChangedToTheDnepr()
        {
            string mainLocationLabel = mainPage.GetTextMainLocationLabel();
            Assert.AreEqual("Днепр", mainLocationLabel);
            //ScenarioContext.Current.Pending();
        }



        [Given(@"https://allo\.ua website is open with light theme")]
        public void GivenHttpsAllo_UaWebsiteIsOpenWithLightTheme()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"User clicks on toogl change site theme")]
        public void WhenUserClicksOnTooglChangeSiteTheme()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"Dark site theme set")]
        public void ThenDarkSiteThemeSet()
        {
            ScenarioContext.Current.Pending();
        }



        [When(@"User clicks on button promotion")]
        public void WhenUserClicksOnButtonPromotion()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"User clicks on button cut in price")]
        public void WhenUserClicksOnButtonCutInPrice()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"User clicks on button contact")]
        public void WhenUserClicksOnButtonContact()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"User clicks on button warranty/return")]
        public void WhenUserClicksOnButtonWarrantyReturn()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"User clicks on button Credit")]
        public void WhenUserClicksOnButtonCredit()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"User clicks on button shipment and payment")]
        public void WhenUserClicksOnButtonShipmentAndPayment()
        {
            ScenarioContext.Current.Pending();
        }

        

        

        

       

        [Then(@"The promotion page is open")]
        public void ThenThePromotionPageIsOpen()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"The cut in price page is open")]
        public void ThenTheCutInPricePageIsOpen()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"The contact page is open")]
        public void ThenTheContactPageIsOpen()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"The warranty/return page is open")]
        public void ThenTheWarrantyReturnPageIsOpen()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"The credit page is open")]
        public void ThenTheCreditPageIsOpen()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"The shipment and payment page is open")]
        public void ThenTheShipmentAndPaymentPageIsOpen()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
