﻿using hw_16.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using OpenQA.Selenium.Interactions;
using TechTalk.SpecFlow;

namespace hw_16.Steps
{
    [Binding]
    public class AuthorizationSteps
    {
        public IWebDriver driver;
        public MainPage mainPage;
        public AuthorizationForm authorizationForm;
        //[BeforeScenario]
        //public void CreateDriver()
        //{
        //    driver = new ChromeDriver(@"C:\");
        //    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMinutes(5);
        //}

        //[Given(@"https://allo\.ua website is open")]
        //public void GivenHttpsAllo_UaWebsiteIsOpen()
        //{
        //    driver.Navigate().GoToUrl("https://allo.ua");
        //    driver.Manage().Window.Maximize();
        //    mainPage = new MainPage(driver);
        //    authorizationForm = new AuthorizationForm(driver);
        //}


        [When(@"User clicks on button authorization")]
        public void WhenUserClicksOnButtonAuthorization()
        {
            mainPage.ClickLoginButtonOnMainPage();
            //ScenarioContext.Current.Pending();
        }
        
        [When(@"User enter in fild e-mail ""(.*)""")]
        public void WhenUserEnterInFildE_Mail(string p0)
        {
            authorizationForm.EmailInAuthorizationFormInput("alya.krasnyanskaya.01@gmail.com");
            //ScenarioContext.Current.Pending();
        }
        
        [When(@"User enter in fild password ""(.*)""")]
        public void WhenUserEnterInFildPassword(string p0)
        {
            authorizationForm.PasswordInAuthorizationFormInput("123456789");
            //ScenarioContext.Current.Pending();
        }

        [When(@"User clicks on button Enter")]
        public void WhenUserClicksOnButtonEnter()
        {
            mainPage =
                authorizationForm.ClickOnEnterAuthorizationFormButton();
            //ScenarioContext.Current.Pending();
        }

        [Then(@"User is logged in")]
        public void ThenUserIsLoggedIn()
        {
           string mainLabelProfileAccount = mainPage.GetTextMainLabelProfileAccountButton();
            Assert.AreEqual("Alina", mainLabelProfileAccount);
            //ScenarioContext.Current.Pending();
        }

        [When(@"User clicks on button registration")]
        public void WhenUserClicksOnButtonRegistration()
        {
            authorizationForm.ClickOnRegistrationAuthorizationFormButton();

            //ScenarioContext.Current.Pending();
        }



        [Then(@"Registration form is open")]
        public void ThenRegistrationFormIsOpen()
        {
            string mainLabelOnButtonRegistration = authorizationForm.GetTextMainLabelRegistrationAuthorizationFormButton();
            Assert.AreEqual("Зареєструватися", mainLabelOnButtonRegistration);
            //ScenarioContext.Current.Pending();
        }
    }
}
