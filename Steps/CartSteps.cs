﻿using hw_16.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System;
using TechTalk.SpecFlow;

namespace hw_16.Steps
{
    [Binding]
    public class CartSteps
    {
        public IWebDriver driver;
        public MainPage mainPage;
        public CartWindow cartWindow;
        //[BeforeScenario]
        //public void CreateDriver()
        //{
        //    driver = new ChromeDriver(@"C:\");
        //    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMinutes(5);

        //}
        [Given(@"https/allo website is open")]
        public void GivenHttpsAlloWebsiteIsOpen()
        {
            driver.Navigate().GoToUrl("https://allo.ua");
            driver.Manage().Window.Maximize();
            mainPage = new MainPage(driver);
            cartWindow = new CartWindow(driver);
        }

        [When(@"User clicks on cart")]
        public void WhenUserClicksOnCart()
        {
            mainPage.ClickCartButtonOnMainPage();
        }

        [Then(@"Cart is empty")]
        public void ThenCartIsEmpty()
        {
            string CartIsEmpty = cartWindow.GetTextMainLabelCart();
            Assert.AreEqual("Ваш кошик порожній.", CartIsEmpty);
        }
    }
}
