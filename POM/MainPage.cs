﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace hw_16.POM
{
    public class MainPage
    {
        private IWebDriver _driver;
        public MainPage(IWebDriver driver)   //конструктор
        {
            this._driver = driver;
        }


        public By DarkThemeMainLabel = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/a/img[1]");
        public By LightkThemeMainLabel = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/a/img[2]");
        public By DarkThemeButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[2]/span[1]");
        public By ThemeSwitcherToggle = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[2]/div");
        public By LightThemeButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[2]/span[2]");

        public By LeaveSmartButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[1]/a");
        public By AlloMoneyButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[2]/a");
        public By AlloUpgradeButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[3]/a");
        public By AlloExchangeButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[4]/a");
        public By CutInPriceButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[5]/a");
        public By PromotionsButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[6]/a");
        public By ContactsButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[2]/div/div[1]");
        public By SearchInput = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[4]/div/div/form/label/input");
        public By LoginButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[5]/div/div/button[1]");
        public By RegistrationButton = By.XPath("/html/body/div[3]/div/div/div[2]/div/ul/li[2]");
        public By RegistrationMainLabel = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/form/button");
        public By CartButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[6]/div/div[1]");
      


        public By BlogButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[1]/a");
        public By FishkaButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[2]/a");
        public By JobPositionsButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[3]/a");
        public By ShopsButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[4]/a");
        public By DeliveryAndPayButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[5]/a");
        public By CreditButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[6]/a");
        public By WarrantyAndRefundButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[7]/a");
        public By ContactButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[8]/a");
        public By LocationButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[1]/div");
        public By LocationKievButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[1]/div[2]/ul/li[1]/span");
        public By LocationLabel = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[1]/div[2]/div/div/div/input");
        public By LocationDneprButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[1]/div[2]/ul/li[4]/span");
       // public By LocationKievLabel = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[1]/div[2]/div/div/div/input");
        public By ProfileAccountButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[5]/div/div/div[1]/span");

        public CartWindow ClickCartButtonOnMainPage()  
        {
            _driver.FindElement(CartButton).Click();
            return new CartWindow(_driver);
        }

        public CartWindow ClickLoginButtonOnMainPage()
        {
            _driver.FindElement(LoginButton).Click();
            return new CartWindow(_driver);
        }

        public IWebElement FindMainLabelProfileAccountButton()
        {
            return _driver.FindElement(ProfileAccountButton);
        }
        public string GetTextMainLabelProfileAccountButton()
        {
            return FindMainLabelProfileAccountButton().Text;
        }

        public MainPage ClickLocationButtonOnMainPage()
        {
            _driver.FindElement(LocationButton).Click();
            return this;
        }
        
        public MainPage ClickLocationKievButtonOnMainPage()
        {
            _driver.FindElement(LocationKievButton).Click();
            return this;
        }

        public MainPage ClickLocationDneprButtonOnMainPage()
        {
            _driver.FindElement(LocationDneprButton).Click();
            return this;
        }

        public IWebElement FindMainLocationLabel()
        {
            return _driver.FindElement(LocationLabel);
        }
        public string GetTextMainLocationLabel()
        {
            return FindMainLocationLabel().Text;
        }
    }
}
