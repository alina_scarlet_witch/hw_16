﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hw_16.POM
{
    public class AuthorizationForm
    {
        private IWebDriver _driver;
        

        public AuthorizationForm(IWebDriver driver)   //конструктор
        {
            this._driver = driver;
        }

        public By EmailOrNumberAuthorizationForm = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/div[1]/div[1]/div[1]/input");
        public By PasswordAuthorizationForm = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/div[1]/div[2]/div[1]/input");
        public By EnterAuthorizationFormButton = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/button");
       
        public By RegistrationAuthorizationFormButton = By.XPath("/html/body/div[3]/div/div/div[2]/div/ul/li[2]");
        public By LabelRegistrationAuthorizationFormButton = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/form/button");
        public AuthorizationForm EmailInAuthorizationFormInput(string email)
        {
            _driver.FindElement(EmailOrNumberAuthorizationForm).SendKeys(email);
            return this;
        }

        public AuthorizationForm PasswordInAuthorizationFormInput(string password)
        {
            _driver.FindElement(PasswordAuthorizationForm).SendKeys(password);
            return this;
        }

        public MainPage ClickOnEnterAuthorizationFormButton()
        {
            _driver.FindElement(EnterAuthorizationFormButton).Click();
            return new MainPage(_driver);
        }

        public AuthorizationForm ClickOnRegistrationAuthorizationFormButton()
        {
            _driver.FindElement(RegistrationAuthorizationFormButton).Click();
            return this;
        }

        public IWebElement FindMainLabelRegistrationAuthorizationFormButton()
        {
            return _driver.FindElement(LabelRegistrationAuthorizationFormButton);
        }
        public string GetTextMainLabelRegistrationAuthorizationFormButton()
        {
            return FindMainLabelRegistrationAuthorizationFormButton().Text;
        }
    } 
}
