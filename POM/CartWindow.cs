﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hw_16.POM
{
    public class CartWindow
    {
        private IWebDriver _driver;

        public CartWindow(IWebDriver driver)   //конструктор
        {
            this._driver = driver;
        }

        public By CartWindowMainLabel = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/p[1]");

        public IWebElement FindMainLabelCart()
        {
            return _driver.FindElement(CartWindowMainLabel);
        }
        public string GetTextMainLabelCart()
        {
            return FindMainLabelCart().Text;
        }
        
    }
}
