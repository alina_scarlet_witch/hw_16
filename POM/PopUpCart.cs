﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace hw_16.POM
{
    class PopUpCart
    {
        private IWebDriver _driver;
        public By mainLabel = By.XPath("/html/body/div[3]/div/div/div[2]/span");
        public By linkToMainPage = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/p[2]/a");
        public By labelCartIsEmpty = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/p[1]");

        
        public PopUpCart(IWebDriver driver)   //конструктор
        {
            this._driver = driver;
        }
    }
}
